'use strict';

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var DonationSchema = new Schema({
	fullname: {
		type: String,
		default: ""
	},
	email: {
		type: String
	},
	mob_num: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true,
		default: 0.00
	},
	created_ts: {
		type: Number,
		required: true,
		default: new Date().getTime()
	},
	last_updated_ts: {
		type: Number,
		required: true,
		default: new Date().getTime()
	},
	status: {
		type: Number,
		required: true,
		default: 1
		/*
			1 = Payment Initiated by Donor
			2 = Payment verified by Admin
			3 = Payment rejected by Admin
		*/
	},
	logs_id: Schema.Types.ObjectId,
}, {collection: 'donations'});

module.exports = mongoose.model('donations', DonationSchema);
