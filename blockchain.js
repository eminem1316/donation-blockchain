var http = require('http');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.raw({ type: '*/*' }));
var router = express.Router();

const SHA256 = require('sha256');

class Block {
    constructor(timestamp, data) {
        this.index = 0;
        this.timestamp = timestamp;
        this.data = data;
        this.previousHash = "0";
        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp + this.data + this.nonce).toString();
    }
}

class Blockchain {
    constructor() {
        this.chain = [this.createGenesis()];
    }

    createGenesis() {
        return new Block(0, "Genesis Block - " + (new Date().toLocaleDateString()), "Genesis block", "0")
    }

    latestBlock() {
        return this.chain[this.chain.length - 1]
    }

    addBlock(newBlock) {
        newBlock.previousHash = this.latestBlock().hash;
        newBlock.index = this.latestBlock().index + 1;
        newBlock.hash = newBlock.calculateHash();
        this.chain.push(newBlock);
    }

    checkValid() {
        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }

        return true;
    }

}

let jsChain = new Blockchain();

router.route('/')
    .get((req, res) => {
        res.set('Access-Control-Allow-Origin', '*');
        res.set('Access-Control-Allow-Credentials', true);
        res.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.json(jsChain);
    })
    .all(function (req, res, next) {
        res.status(405).end();
    });

router.route('/add_block')
    .post((req, res) => {
        var data = req.body;
        jsChain.addBlock(new Block(new Date(), data));
        res.set('Access-Control-Allow-Origin', '*');
        res.set('Access-Control-Allow-Credentials', true);
        res.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.end();
    })
    .all(function (req, res, next) {
        res.status(405).end();
    });

app.use('/blockchain', router);
http.createServer(app).listen(3007);
console.log('Blockchain API instance is live on port 3007');