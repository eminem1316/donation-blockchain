var http = require('http');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose/');

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.raw({ type: '*/*' }));
var router = express.Router();

const Donation = require('./schemas/donations');

// Arbitrary Constants
const CONSTANTS = {
	DB_HOST: '18.219.51.76',
	DB_PORT: 27017,
	DB_USER: 'admin',
	DB_PASSWORD: 'Georgia123#',
	DB_DATABASE: 'donation_db',
	SELF_IP: ''
};

// Arbitrary Functions
function maskMobileNumber(mob_num) {
	return mob_num.replace(/\d(?=\d{4})/g, "X");
}

// Setting DB connection
const dbOptions = { user: CONSTANTS.DB_USER, pass: CONSTANTS.DB_PASSWORD, useNewUrlParser: true };
mongoose.connect("mongodb://" + CONSTANTS.DB_HOST + ":" + CONSTANTS.DB_PORT + "/" + CONSTANTS.DB_DATABASE + "?authSource=admin", dbOptions).then(() => {
	console.log("Mongoose connected successfully.");
}, err => {
	console.log("Error. Shooting Email for error: " + err);
});

// http server for the blockchain
router.route('/main/submit_donation')
	.post((req, res) => {
		res.set('Access-Control-Allow-Origin', '*');
		res.set('Access-Control-Allow-Credentials', true);
		res.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
		res.set('Access-Control-Allow-Headers', 'Content-Type');
		
		// Adding entry to the database
		var donation = new Donation();
		donation.fullname = req.body.full_name;
		donation.email = req.body.email;
		donation.mob_num = req.body.mob_num;
		donation.amount = parseInt(req.body.amount).toFixed(2);
		donation.save((err, saved_donation) => {
			if (err) {
				res.set({ err: err });
				res.status(503).end();
			} else {
				// Adding data to the public blockchain
				const bc_data = {
					full_name: req.body.full_name,
					mobile_number: req.body.mob_num,
					amount: parseFloat(req.body.amount).toFixed(2)
				};
				require('request').post({
					url: 'http://localhost:3007/blockchain/add_block',
					method: 'POST',
					json: true,
					body: bc_data
				}, (err, resp, body) => {
					if (err) {
						res.status(500).end();
					} else {
						res.end();
					}
				});
			}
		});
	})
	.all(function (req, res, next) {
		res.status(405).end();
	});

router.route('/main/display_blockchain')
	.get((req, res) => {
		res.set('Access-Control-Allow-Origin', '*');
		res.set('Access-Control-Allow-Credentials', true);
		res.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
		res.set('Access-Control-Allow-Headers', 'Content-Type');
		
		http.get('http://localhost:3007/blockchain/', (resp) => {
			let data = '';

			// A chunk of data has been recieved.
			resp.on('data', (chunk) => {
				data += chunk;
			});

			// The whole response has been received. Print out the result.
			resp.on('end', () => {
				var updatedChain = (JSON.parse(data)).chain;
				updatedChain.map((block) => {
					if (block.data.mobile_number) {
						block.data.mobile_number = maskMobileNumber(block.data.mobile_number);
					}
				});
				res.json({ chain: updatedChain });
			});
		});
	})
	.all(function (req, res, next) {
		res.status(405).end();
	});

router.route('/main/display_donations')
	.get((req, res) => {
		Donation.find({}).sort({ created_ts: -1 }).exec((err, found_donations) => {
			res.set('Access-Control-Allow-Origin', '*');
			res.set('Access-Control-Allow-Credentials', true);
			res.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
			res.set('Access-Control-Allow-Headers', 'Content-Type');
			if (err) {
				res.set({ err: err });
				res.status(503).end();
			} else {
				var donations = found_donations;
				donations.map((donation) => {
					donation.mob_num = maskMobileNumber(donation.mob_num);
				});
				res.json({ donations: donations });
			}
		});
	})
	.all(function (req, res, next) {
		res.status(405).end();
	});

router.route('/admin/display_list')
	.get((req, res) => {
		Donation.find({}).sort({ created_ts: -1 }).exec((err, found_donations) => {
			res.set('Access-Control-Allow-Origin', '*');
			res.set('Access-Control-Allow-Credentials', true);
			res.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
			res.set('Access-Control-Allow-Headers', 'Content-Type');
			
			if (err) {
				res.set({ err: err });
				res.status(503).end();
			} else {
				res.json({ donations: found_donations });
			}
		});
	})
	.all(function (req, res, next) {
		res.status(405).end();
	});

router.route('/admin/update_status')
	.post((req, res) => {
		Donation.findOne({_id: req.body.id}).exec((err, found_donation) => {
			res.set('Access-Control-Allow-Origin', '*');
			res.set('Access-Control-Allow-Credentials', true);
			res.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
			res.set('Access-Control-Allow-Headers', 'Content-Type');
			
			if (err) {
				res.set({ err: err });
				res.status(503).end();
			} else {
				if(!found_donation){
					res.set({ err: "Donation not found." });
					res.status(404).end();
				} else {
					found_donation.status = parseInt(req.body.status);
					found_donation.last_updated_ts = new Date().getTime();
					found_donation.save((err, saved_dn)=>{
						if (err) {
							res.set({ err: err });
							res.status(503).end();
						} else {
							res.end();
						}
					});
				}
			}
		});
	})
	.all(function (req, res, next) {
		res.status(405).end();
	});

app.use('/donate', router);
http.createServer(app).listen(3002);
console.log('API instance is live on port 3002');
